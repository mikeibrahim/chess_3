**Quick Overview**
* This project is my extension of the Gunn 2021 Hackathon, an event where I worked alongside Yiunfan, Roger, and Victor to produce "Well Chess, but Actually No" -  an online chess game designed to break your mind with configurable and irregular chess rules/strategies.

* This game was built and tested in the Unity Game Engine, a powerful software that allows for an engaging and advanced visual experience.

* All of my code is architected and written in C#


**Where to play this game**: https://mikeibrahim.itch.io/chess-3


* NOTE: This is a MULTIPLAYER game, so try and connect with a friend to play against.

* (If all else fails, you can duplicate the game tab and play by yourself).
