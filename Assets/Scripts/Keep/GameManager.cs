using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using System.Linq;
using Hashtable = ExitGames.Client.Photon.Hashtable;


// Handles Game Logic
public class GameManager : MonoBehaviourPunCallbacks { 
	public static GameManager Inst;
	private Hashtable playerProps = new Hashtable();
	private PhotonView PV;
	[SerializeField] private Tile tileGo;
	[SerializeField] private Piece peiceGo;
	[SerializeField] private Transform tileHolder;
	[SerializeField] private Transform[] graveyards;
	private Tile[,] board;
	private static int boardSize = -1; // change for game config
	private Tile clickedTile;
	private bool isHost;
	private bool myTurn;
	private bool endOfGame;


	public int GetBoardSize() => board.Length;
	public void SetClickedTile(Tile tile) => clickedTile = tile;
	public Tile GetClickedTile() => clickedTile;

#region Start
	private void Awake() => InititateFields();
	private void InititateFields() {
		Inst = this;
		PV = GetComponent<PhotonView>();
		isHost = PhotonNetwork.IsMasterClient;
		myTurn = isHost;
		boardSize = GameConfiguration.Instance.GetBoardSize();
		playerProps["Player"] = GetPlayerIndex() <= 1;
		PhotonNetwork.LocalPlayer.SetCustomProperties(playerProps);
		SetTurnText();
		ResetChaosTimer();
		if (GetPlayerIndex() == 1)
			(graveyards[0], graveyards[1]) = (graveyards[1], graveyards[0]);
		UpdateSpectatorCount();
	}
	private void Start() { if (isHost) { InitiateBoard(boardSize); } }// Create the game board if you are the host
#endregion

#region Game Logic
	// Makes the pieces that go into the tiles, which are in the board[,]
	private void InitiatePieces() { 
		for (int x = 0; x < GameConfiguration.Instance.GetPieceSetup().GetLength(0); x++) { // Gets me the x position
			for (int y = 0; y < GameConfiguration.Instance.GetPieceSetup().GetLength(1); y++) { // Gets me the y position
				InitiatePiece(x, y, x, y); // Creates a piece with the corresponding position

				int xOther = board.GetLength(0) - 1 - x, yOther = board.GetLength(0) - 1 - y;
				InitiatePiece(x, y, xOther, yOther); // Creates a piece on opposite end
			}
		}
	}
	// Makes a single piece with the given arguments
	private void InitiatePiece(int xType, int yType, int x, int y) {
		int type = GameConfiguration.Instance.GetPieceSetup()[xType, yType]; // Getting the type at that position
		if (Famine(type)) { return; }
		
		Vector2 spawnPos = new Vector2(x, y); // Getting the spawnpoint
		Color c = y < board.GetLength(0)/2 ? Color.white : Color.gray;  // Color based on which half of the board the piece is in
		Piece p = Instantiate(peiceGo); // Creating the physical piece with the desired spawnpoint
		board[x, y].SetPiece(p); // Placing the piece on its corresponding tile

		type = SwapKingAndQueen(type, c);
		p.SetUp(type, c); // setting up the class and color of the piece

		if (GetPlayerIndex() == 1) { p.transform.Rotate(new Vector3(0, 0, 180)); } // rotating all the pieces if you are p2
	}
	private void ShowMoves(Tile tile, bool getMove) {		
		foreach ((int, int) move in GetMoves(tile, getMove))
			board[move.Item1, move.Item2].SetActive(true, getMove ? Color.green : Color.red);
	}
	private (int, int)[] GetMoves(Tile tile, bool move) {
		Piece piece = tile.GetPiece();
		// piece = Nigerundayo(piece, withRules);
		(int, int)[] moveVectors = piece.GetMoves(move); // find all of the regular vectors
		(int, int)[] propagatedMoves = PropagateMoves(piece, moveVectors, move);
		return propagatedMoves;
	}
	// returns moves that have been propagated in all directions
	private (int, int)[] PropagateMoves(Piece piece, (int, int)[] normalMoves, bool move) {
		List<(int, int)> propagatedMoves = new List<(int, int)>();
		(int, int) tilePos = ((int)piece.GetPos().x, (int)piece.GetPos().y);

		foreach ((int, int) normalMove in normalMoves) { // for every normal move
			int mult = 1;
			(int, int) multipliedMove = ((normalMove.Item1 * mult) + tilePos.Item1, (normalMove.Item2 * mult) + tilePos.Item2);
			
			if (move) { // Finding the moveVectors
				while (InBounds(multipliedMove) && !HitPiece(multipliedMove) && (AppliesToMoveStyle(piece, multipliedMove, mult) || DoublePawnMove(piece, mult))) {
					propagatedMoves.Add(multipliedMove);
					mult++;
					multipliedMove = ((normalMove.Item1 * mult) + tilePos.Item1, (normalMove.Item2 * mult) + tilePos.Item2);
				}
			} else { // finding the attackVectors
				while (InBounds(multipliedMove) && !HitColorPiece(multipliedMove, piece.GetColor(), true) && AppliesToMoveStyle(piece, multipliedMove, mult)) {
					if (HitColorPiece(multipliedMove, piece.GetColor(), false)) {
						propagatedMoves.Add(multipliedMove); break;
					}
					mult++;
					multipliedMove = ((normalMove.Item1 * mult) + tilePos.Item1, (normalMove.Item2 * mult) + tilePos.Item2);
				}
			}
		}
		return propagatedMoves.ToArray();
	}
	// Logic for when a tile gets clicked
	public void ClickedTile(Tile tile) {
		if (!CanClickTile()) { return; } // only interact with tiles if it is your turn or if not spectator

		if (tile.GetActive()) { // if the tile was green & clicked -> meaning that you move the before clicked piece
			MovePiece(tile.GetPos(), clickedTile.GetPos());
			ResetTiles(null); // reset every single tile
			EndTurn();
			ResetChaosTimer();
		} else if (tile.GetPiece() && PieceIsMyColor(tile.GetPiece())) { // if you have a piece && the tile isnt green
			ResetTiles(tile); // resetting all of the tiles to be deactivated
			if (tile != clickedTile) { // If the tile wasnt clicked before
				// Nigerundayo(tile.GetPiece());
				ShowMoves(tile, true); // show where the piece can go
				ShowMoves(tile, false);
				clickedTile = tile;
			} else { // If the tile was clicked before
				clickedTile = null; // no tile shall be clicked/"active"
			}
		}
	}
	private void ResetTiles(Tile tile) {
		foreach (Tile t in board) {
			t.SetActive(false);
			if (t != tile) { t.SetClicked(false); }
		}
	}
	public int GetPlayerIndex() {
		List<Player> playerlist = PhotonNetwork.PlayerList.ToList();
		int index = playerlist.IndexOf(PhotonNetwork.LocalPlayer);
		return index;
	}
	public int GetPlayerIndex(Player player) {
		List<Player> playerlist = PhotonNetwork.PlayerList.ToList();
		int index = playerlist.IndexOf(player);
		return index;
	}
	public void SendToGraveyard(Piece piece) {
		if (HeroesNeverDie(piece)) { return; }
		
		int graveyardNum = piece.GetColor() == Color.gray ? 0 : 1;
		piece.transform.SetParent(graveyards[graveyardNum]);
		if (GetPlayerIndex() == 1) { piece.transform.Rotate(0, 0, 180); }
		if (piece.GetPieceName() == Pieces.K) { EndGame(!PieceIsMyColor(piece)); }
		Destroy(piece); // remove the piece component
	}
	public void EndGame(bool win) {
		endOfGame = true;
		GameUI.Inst.EndGame(win);
		if (PhotonNetwork.IsMasterClient) {
			PhotonNetwork.CurrentRoom.IsOpen = false;
		}
	}
	private Piece GetPiece(bool mySide, int pieceType) {
		foreach (Tile tile in board) {
			Piece piece = tile.GetPiece();
			if (piece && (mySide ? PieceIsMyColor(piece) : !PieceIsMyColor(piece)) && piece.GetPieceName() == pieceType) // if that tile is a king
				return piece;
		}
		return null;
	}
	private void ReplacePiece(Piece piece, int replaceType) {
		int x = (int)piece.GetPos().x, y = (int)piece.GetPos().y;

		Piece newPiece = Instantiate(peiceGo);
		newPiece.SetUp(replaceType, piece.GetColor());
		board[x, y].SetPiece(newPiece);
		if (GetPlayerIndex() == 1) { newPiece.transform.Rotate(new Vector3(0, 0, 180)); }
		Destroy(piece.gameObject);
	}
	private void TryPromote(Piece piece) {
		if ((piece.GetPieceName() == Pieces.P || Inauguration(piece)) && (OnEdgeOfBoard(piece) || QueenFabrication(piece))) {
			ReplacePiece(piece, Pieces.Q);
		}
	}
	private int SwapKingAndQueen(int pieceType, Color pieceColor) {
		if (pieceType == Pieces.K && pieceColor == Color.gray)
			pieceType = Pieces.Q;
		else if (pieceType == Pieces.Q && pieceColor == Color.gray)
			pieceType = Pieces.K;
		return pieceType;
	}
	public void SetTurnText() {
		GameUI.Inst.SetPlayerStatus(myTurn || Chaos());
	}
	public void UpdateSpectatorCount() {
		int numSpectetors = 0;
		foreach (Player player in PhotonNetwork.PlayerList) {
			if (GetPlayerIndex(player) >= 2)
				numSpectetors++;
		}
		GameUI.Inst.SetSpectators(numSpectetors);
	}
#endregion

#region RPC's
	// Makes the tiles that go into the board[,]
	[PunRPC] public void RPC_InitiateBoard(int size) {
		board = new Tile[size, size]; // Initiating board with inputted dimensions

		for (int x = 0; x < size; x++) { // Gets me the x position
			for (int y = 0; y < size; y++) { // Gets me the y position
				Vector2 spawnPos = new Vector2(x, y); // Corresponding coordinates
				Tile t = Instantiate(tileGo, spawnPos, Quaternion.identity);
				t.transform.SetParent(tileHolder, true);
				t.Setup((x + y) % 2 == 0 ? Color.white : Color.gray); // tile color
				board[x, y] = t;
			}
		}

		GameCam.Inst.CenterOnBoard(size, isHost); // Centers the camera

		InitiatePieces(); // Creates the peices
	}
	private void InitiateBoard(int size) => PV.RPC("RPC_InitiateBoard", RpcTarget.AllBuffered, size);
	// Moving the previously clicked tile's piece to the newly clicked one
	[PunRPC] public void RPC_MovePiece(Vector2 tilePos, Vector2 clickedTilePos) {
		Tile tile = board[(int)tilePos.x, (int)tilePos.y], clickedTile = board[(int)clickedTilePos.x, (int)clickedTilePos.y]; // Gets the tiles in the board array

		tile.TakePiece();; // removing previous piece
		tile.SetPiece(clickedTile.GetPiece()); // replacing the piece with the clicked tile one
		clickedTile.SetPiece(null); // The previously clicked tile now has no piece
		
		TryPromote(tile.GetPiece());
		Nigerundayo(tile.GetPiece());
		Deadeye(tile.GetPiece());
	}
	private void MovePiece(Vector2 tilePos, Vector2 clickedTilePos) => PV.RPC("RPC_MovePiece", RpcTarget.AllBuffered, tilePos, clickedTilePos);
	[PunRPC] private void RPC_EndTurn() { myTurn = !myTurn;  SetTurnText(); }
	private void EndTurn() => PV.RPC("RPC_EndTurn", RpcTarget.AllBuffered);
#endregion

#region Rules
	private bool Chaos() {
		if (!HasRule(Rules.Chaos))
			return false;
		else
			myTurn = false;
		return GameUI.Inst.GetTurnTime() <= 0;
	}
	private void ResetChaosTimer() { if (HasRule(Rules.Chaos)) { GameUI.Inst.ResetTurnTime(); } }
	private void Deadeye(Piece attackingPiece) {
		if (!HasRule(Rules.Deadeye)) { return; } // if the rule is not active

		if (InCheck(!PieceIsMyColor(attackingPiece))) {
			EndGame(PieceIsMyColor(attackingPiece));
		}
	}
	private bool Famine(int tyoe) {
		if (!HasRule(Rules.Famine)) { return false; } // if the rule is not active

		return tyoe == Pieces.P;
	}
	private bool HeroesNeverDie(Piece piece) {
		if (!HasRule(Rules.HeroesNeverDie)) { return false; } // if the rule is not active

		Vector2 spawnPoint = piece.GetSpawnPoint();
		Tile spawnPointTile = board[(int)spawnPoint.x, (int)spawnPoint.y];
		if (!spawnPointTile.GetPiece()) {
			spawnPointTile.SetPiece(piece);
			return true; // 
		}
		return false;
	}
	private void Nigerundayo(Piece piece) {
		if (!HasRule(Rules.Nigerundayo)) { return; }

		bool kingSide = !PieceIsMyColor(piece);
		Piece king = GetPiece(kingSide, Pieces.K);
		king.SetMoveTupe(InCheck(kingSide) ? Pieces.Q : Pieces.K);
	}
	private bool Inauguration(Piece piece) => HasRule(Rules.Inauguration) && piece.GetPieceName() != Pieces.K;
	private bool Onward(Piece piece, bool backMove) => (!HasRule(Rules.Onward) && backMove) || (HasRule(Rules.Onward) && (piece.GetPieceName() == Pieces.K || piece.GetPieceName() == Pieces.Q));
	private bool QueenFabrication(Piece piece) => HasRule(Rules.QueenFabrication) && ((piece.GetColor() == Color.white && piece.GetPos().y >= (board.GetLength(0))/2) || (piece.GetColor() == Color.gray && piece.GetPos().y <= (board.GetLength(0)-1)/2));
#endregion

#region Conditions
	private bool InBounds((int, int) pos) => pos.Item1 >= 0 && pos.Item2 >= 0 && pos.Item1 < board.GetLength(0) && pos.Item2 < board.GetLength(1);
	private bool HitPiece((int, int) pos) => board[pos.Item1, pos.Item2].GetPiece();
	private bool HitColorPiece((int, int) pos, Color pieceColor, bool sameColor) {
		Piece piece = board[pos.Item1, pos.Item2].GetPiece();
		return piece && (sameColor ? pieceColor == piece.GetColor() : pieceColor != piece.GetColor());
	}
	public bool PieceIsMyColor(Piece piece) => (isHost && piece.GetColor() == Color.white) || (!isHost && piece.GetColor() == Color.gray);
	private bool AppliesToMoveStyle(Piece piece, (int, int) pos, int mult) {
		bool infiniteMovePiece = piece.GetMoveStyles().Item1;
		bool behindMovePiece = piece.GetMoveStyles().Item2;
		behindMovePiece = Onward(piece, behindMovePiece);

		bool behindMove = (pos.Item2 <= piece.GetPos().y && piece.GetColor() == Color.white) || (pos.Item2 >= piece.GetPos().y && piece.GetColor() == Color.gray);
		bool infiniteMove = mult > 1;

		return (!behindMove || behindMovePiece) && (!infiniteMove || infiniteMovePiece);
	}
	private bool CanClickTile() => (Chaos() || myTurn) && GetPlayerIndex() <= 1 && !endOfGame;
	public bool HasRule(int index) => GameConfiguration.Instance.HasRule(index);
	private bool InCheck(bool mySide) { // bool myside is checking which side is in check
		Piece king = GetPiece(mySide, Pieces.K);
		if (!king) { return false; }
		Vector2 kingPos = king.GetPos();

		foreach (Tile tile in board) {
			Piece piece = tile.GetPiece();
			if (piece && (mySide ? !PieceIsMyColor(piece) : PieceIsMyColor(piece))) {
				List<(int, int)> possibleMoves = new List<(int, int)>(GetMoves(tile, false));
				if (possibleMoves.Contains(((int)kingPos.x, (int)kingPos.y))) 
					return true;
			}
		}
		return false;
	}
	private bool DoublePawnMove(Piece piece, int mult) => piece.GetPieceName() == Pieces.P && mult == 2 && piece.GetSpawnPoint() == piece.GetPos();
	private bool OnEdgeOfBoard(Piece piece) => (piece.GetColor() == Color.white && piece.GetPos().y == board.GetLength(0)-1) || (piece.GetColor() == Color.gray && piece.GetPos().y == 0);
#endregion

#region Callbacks
	public override void OnPlayerEnteredRoom(Player otherPlayer) {
		UpdateSpectatorCount();
	}
	public override void OnPlayerLeftRoom(Player otherPlayer) {
		if ((bool)otherPlayer.CustomProperties["Player"] && !endOfGame) {
			EndGame(true);
		}
		UpdateSpectatorCount();
	}
#endregion
}