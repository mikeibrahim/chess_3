using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RuleInGame : MonoBehaviour {
	int ruleIndex;
	[SerializeField] private Image ruleThumbnail;
	[SerializeField] private TMP_Text text_ruleName;
	[SerializeField] private TMP_Text text_description;

	public void SetUp((string, string) rule, Sprite ruleThumbnaul) {
		text_ruleName.text = rule.Item1;
		text_description.text = rule.Item2;
		ruleThumbnail.sprite = ruleThumbnaul;
	}
}