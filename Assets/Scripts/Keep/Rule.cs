using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public static class Rules {
	public static int  	Chaos = 			0,
						Deadeye = 			1,
						Famine = 			2,
						HeroesNeverDie = 	3,
						Nigerundayo = 		4,
						Inauguration = 		5,
						Onward = 			6,
						QueenFabrication = 	7;
}

public class Rule : MonoBehaviour { 
	int ruleIndex;
	[SerializeField] private Image ruleThumbnail;
	[SerializeField] private TMP_Text text_ruleName;
	[SerializeField] private TMP_Text text_activated;
	[SerializeField] private TMP_Text text_description;
	Button toggleActive;
	bool isActivated = false;
	
	public void SetUp(string name, string description, Sprite thumbnail) {
		text_ruleName.text = name;
		text_description.text = description;
		ruleThumbnail.sprite = thumbnail;
		toggleActive = GetComponent<Button>();
	}

    void Start() {
        // ruleThumbnail.sprite = Thumbnail.Instance.GetRuleThumbnail(ruleIndex);
		toggleActive.onClick.AddListener(delegate {  ToggleActive();  }); // toggle activation
    }

	private void ToggleActive() {
		isActivated = !isActivated;
		GameConfiguration.Instance.AddRule(ruleIndex, isActivated);
	}

	public int GetRuleIndex() => ruleIndex;
	public void SetRuleIndex(int index) => ruleIndex = index;
}