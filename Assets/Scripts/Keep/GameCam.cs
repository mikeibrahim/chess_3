using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCam : MonoBehaviour {
	public static GameCam Inst;

	void Awake() => Inst = this;

	public void CenterOnBoard(int size, bool player1) {
		transform.position = new Vector3((size-1f)/2, (size-1f)/2, -10);
		GetComponent<Camera>().orthographicSize = size * 0.7f;
		if (GameManager.Inst.GetPlayerIndex() == 1) { transform.Rotate(new Vector3(0, 0, 180)); } // rotating camera if p2
	}
}
