using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Photon.Pun;

// Getting the rules
public class GameConfiguration : MonoBehaviourPunCallbacks {
	public static GameConfiguration Instance;
	PhotonView PV;
	[SerializeField] private Transform deactivatedRuleHolder;
	[SerializeField] private Transform activatedRuleHolder;
	[SerializeField] private GridLayoutGroup boardPreview;
	[SerializeField] private Image tilePreview;
	[SerializeField] private GameObject ruleGO;

	private (string, string)[] rules = {("Chaos",				"No Turns, 3s move cooldown"),
										("Deadeye",				"Check = Checkmate"),
										("Famine", 				"Pawns are gone"),
										("Heroes Never Die", 	"Pieces respawn at their origional spawn point"),
										("Nigerundayo",			"Kings move like queens in check"),
										("Inauguration",		"All pieces can promote, except king"),
										("Onward",				"Pieces can't move back, except King and Queens"),
										("Queen Fabrication", 	"Pawn promotions occur halfway")};
	
	private static int[,] pieceSetup = {	{ Pieces.R, Pieces.P },
											{ Pieces.N, Pieces.P },
											{ Pieces.B, Pieces.P },
											{ Pieces.Q, Pieces.P },
											{ Pieces.K, Pieces.P },
											{ Pieces.B, Pieces.P },
											{ Pieces.N, Pieces.P },
											{ Pieces.R, Pieces.P }	};

	private List<int> activeRules = new List<int>();
	[SerializeField] private SlideBar slider_boardSize;
	[SerializeField] private TMP_Text text_boardSize;


	void Awake() {
		InitiateFields();
	}
	private void InitiateFields() {
		Instance = this;
		PV = GetComponent<PhotonView>();

		slider_boardSize.GetComponent<Slider>().onValueChanged.AddListener(delegate{  text_boardSize.text = slider_boardSize.GetValue().ToString(); ResizeBoardPreview(slider_boardSize.GetValue()); });
		slider_boardSize.SetMaxValue(16); // Setting max range for board size
		slider_boardSize.SetMinValue(8);
	}
	void Start() {
		CreateRules();
	}
	private void CreateRules() {
		for (int i = 0; i < rules.Length; i++) {
			Rule r = Instantiate(ruleGO, deactivatedRuleHolder).GetComponent<Rule>();
			r.SetRuleIndex(i);
			r.SetUp(rules[i].Item1, rules[i].Item2, Thumbnail.Instance.GetRuleThumbnail(i));
		}
	}

	public int GetBoardSize() => slider_boardSize.GetValue();
	public List<int> GetActiveRules() => activeRules;
	public (string, string)[] GetRules() => rules;
	public bool HasRule(int index) => activeRules.Contains(index);
	public void AddRule(int ruleIndex, bool adding) { 
		if (PhotonNetwork.IsMasterClient)
			PV.RPC("RPC_AddRule", RpcTarget.AllBuffered, ruleIndex, adding);
	}
	[PunRPC] private void RPC_AddRule(int ruleIndex, bool adding) {
		foreach (Transform trans in adding ? deactivatedRuleHolder : activatedRuleHolder) {
			if (trans.GetComponent<Rule>()?.GetRuleIndex() == ruleIndex) {
				trans.SetParent(adding ? activatedRuleHolder : deactivatedRuleHolder);
				if (adding)
					activeRules.Add(ruleIndex);
				else
					activeRules.Remove(ruleIndex);
			}
		}
	}

	private void ResizeBoardPreview(int size) {
		if (PhotonNetwork.IsConnected)
			PV.RPC("RPC_ResizeBoardPreview", RpcTarget.AllBuffered, size);
		else
			Local_ResizeBoardPreview(size);
	}
	[PunRPC] private void RPC_ResizeBoardPreview(int size) {
		Local_ResizeBoardPreview(size);
	}
	private void Local_ResizeBoardPreview(int size) {
		foreach (Transform trans in boardPreview.transform)
			Destroy(trans.gameObject);
		
		boardPreview.cellSize = new Vector2(500/size, 500/size);
		for (int x = 0; x < size; x++) {
			for (int y = 0; y < size; y++) {
				Image tilePreviewImage = Instantiate(tilePreview, boardPreview.transform);
				tilePreviewImage.color = (x + y) % 2 == 0 ? Color.white : Color.gray;
			}
		}
	}

	public int[,] GetPieceSetup() => pieceSetup;

	[PunRPC] private void RPC_ApplySettings(int[] activeRules) => this.activeRules = new List<int>(activeRules);
	public void ApplySettings() => PV.RPC("RPC_ApplySettings", RpcTarget.AllBuffered, activeRules.ToArray());

	public override void OnConnected() {
		
	}
}