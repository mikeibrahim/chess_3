using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public struct pieceType {
	public int pieceName;
// 			Move 		  Attack
	public ((int, int)[], (int, int)[]) moveVectors;
//		   Infin Behind Ghost
    public (bool, bool) moveStyles;

	public pieceType(int pn, ((int, int)[], (int, int)[]) mv, (bool, bool) ms) {
		this.pieceName = pn;
		this.moveVectors = mv;
        this.moveStyles = ms;
	}
}

public class Pieces {
	public static int P = 0, R = 1, N = 2, B = 3, Q = 4, K = 5;

	public static pieceType[] pieces = {
		new pieceType(P,	(new(int, int)[]{ (0, 1) }, 			new(int, int)[]{ (1, 1) } ), 	(false, false)),
		new pieceType(R,	(new(int, int)[]{ (0, 1) }, 			null), 							(true, 	true)),
		new pieceType(N,	(new(int, int)[]{ (1, 2) }, 			null), 							(false, true)),
		new pieceType(B,	(new(int, int)[]{ (1, 1) }, 			null), 							(true, 	true)),
		new pieceType(Q,	(new(int, int)[]{ (0, 1), (1, 1) }, 	null), 							(true, 	true)),
		new pieceType(K,	(new(int, int)[]{ (0, 1), (1, 1) }, 	null), 							(false, true)),
	};
}

public class Piece : MonoBehaviour {
	private int pieceName;
	private ((int, int)[], (int, int)[]) moveVectors;
    private (bool, bool) moveStyles;
	private Color pieceColor;
	private Vector2 spawnPoint;

	public void SetUp(int type, Color pieceColor) {
		pieceType currentType = Pieces.pieces[type];
		pieceName = currentType.pieceName;
		moveVectors = currentType.moveVectors;
		moveStyles = currentType.moveStyles;
		this.pieceColor = pieceColor;

		GetComponent<Image>().sprite = pieceColor == Color.white ? 
										Thumbnail.Instance.GetWhiteThumbnail(pieceName) : 
										Thumbnail.Instance.GetBlackThumbnail(pieceName);
		spawnPoint = transform.position;
		// print("spawnPoint: "+spawnPoint);
	}
	// Returns all of the moves from a single vector
	public (int, int)[] GetMoves(bool move) {
		List<(int, int)> moveList = new List<(int, int)>();
		foreach ((int, int) moveVector in (move || moveVectors.Item2 == null) ? moveVectors.Item1 : moveVectors.Item2) 
			for (int varientX = 0; varientX < 2; varientX++)  // positive/negative x
				for (int varientY = 0; varientY < 2; varientY++)  // positive/negative y
					for (int varientSwap = 0; varientSwap < 2; varientSwap++) { // x/y OR y/x
						int finalX = varientX == 0 ? moveVector.Item1 : -moveVector.Item1;
						int finalY = varientY == 0 ? moveVector.Item2 : -moveVector.Item2;
						(int, int) varientMoveVector = varientSwap == 0 ? (finalX, finalY) : (finalY, finalX);
						if (!moveList.Contains(varientMoveVector)) { moveList.Add(varientMoveVector); }
					}
		
		return moveList.ToArray();
	}
	public void SetMoveTupe(int type) {
		pieceType currentType = Pieces.pieces[type];
		moveVectors = currentType.moveVectors;
		moveStyles = currentType.moveStyles;
	}
	public int GetPieceName() => pieceName;
	public Vector2 GetPos() => transform.position;
	public (int, int)[] GetAttacks() => moveVectors.Item2;
	public (bool, bool) GetMoveStyles() => moveStyles;
	public Color GetColor() => pieceColor;
	public Vector2 GetSpawnPoint() => spawnPoint;
}