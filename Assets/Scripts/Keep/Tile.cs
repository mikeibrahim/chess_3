using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using TMPro;

public class Tile : MonoBehaviour {
	private Image image = null;
	private Piece piece;
	private Color tileColor;
	private Color activeColor = Color.green;
	private bool clicked;
	private bool active;

    public void Setup(Color tileColor) {
		image = GetComponent<Image>();
		this.tileColor = tileColor;
		image.color = tileColor;
	}

	private void Awake() {
		GetComponent<Button>().onClick.AddListener(delegate{ OnClick(); }); // whenever the button is pushed
	}

	public Vector2 GetPos() => transform.position;

	public Piece GetPiece() => this.piece;
	public void SetPiece(Piece piece) {
		this.piece = piece;
		if (piece) {
			piece.transform.position = transform.position;
			piece.transform.SetParent(transform, true);
		}
	}

	public void TakePiece() {
		if (piece) {
			GameManager.Inst.SendToGraveyard(piece);
			piece = null;
			// Destroy(piece.gameObject);
		}
	}

	public bool GetActive() => this.active;
	// Showing the green -> meaning you can transfer a piece there
	public void SetActive(bool active) {
		this.active = active;
		GetComponent<Image>().color = active ? activeColor : tileColor;
	}
	public void SetActive(bool active, Color color) {
		this.active = active;
		GetComponent<Image>().color = active ? color : tileColor;
	}

	public void SetClicked(bool clicked) => this.clicked = clicked;
	private void OnClick() {
		GameManager.Inst.ClickedTile(this); // when clicked, send tile to game manager to handle logic
	}
}
