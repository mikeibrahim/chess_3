using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Realtime;

public class RoomListItem : MonoBehaviour {
	public RoomInfo info;
	[SerializeField] private TMP_Text text_roomName;


	public void SetUp(RoomInfo info) {
		this.info = info;
		text_roomName.text = info.Name;
	}

	public void OnClick() {
		Launcher.Instance.JoinRoom(info);
	}

	public RoomInfo GetInfo() => info;
}
