using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using System.Linq;

public class Launcher : MonoBehaviourPunCallbacks  {
	public static Launcher Instance;
	[SerializeField] private TMP_InputField inputField_playerName;
	[SerializeField] private TMP_InputField inputField_roomName; // the name of the room
	[SerializeField] private TMP_Text text_roomName; // the title of the room
	[SerializeField] private Transform panel_roomListHolder; // Showing the Rooms
	[SerializeField] private GameObject prefab_roomListItem; // a single room gameobject
	[SerializeField] private Transform panel_playerListHolder; // Showing the Players
	[SerializeField] private GameObject prefab_playerListItem; // a single player gameobject
	[SerializeField] private GameObject button_startGame; // start game button
	[SerializeField] private GameObject panel_boardSize; // the gameobject to hold the rules
	private List<RoomInfo> myRoomInfo = new List<RoomInfo>();

	void Awake()  {
		Instance = this;
	}
    void Start()  {
		PhotonNetwork.ConnectUsingSettings();
		MenuManager.Instance.SetMenu(MenuManager.LoadingMenu); // start off with the loading screen
		inputField_playerName.text = PlayerPrefs.GetString("playername");
    }
	public override void OnConnectedToMaster()  {
		PhotonNetwork.JoinLobby(); // Join the mini-server
		PhotonNetwork.AutomaticallySyncScene = true; // makes it so that the scenes are loaded at the same time for all players
	}
	public override void OnJoinedLobby()  {
		MenuManager.Instance.SetMenu(MenuManager.TitleMenu);
	}
	public void CreateRoom()  {
		if (string.IsNullOrEmpty(inputField_roomName.text) || myRoomInfo.Any(i=>i.Name == inputField_roomName.text))  {
			return;
		}
		SetPlayerName();
		PhotonNetwork.CreateRoom(inputField_roomName.text);
		MenuManager.Instance.SetMenu(MenuManager.LoadingMenu);
	}
	public override void OnJoinedRoom()  {
		button_startGame.GetComponent<Button>().interactable = CanStartGame(); // can only play if there are 2+ people
		MenuManager.Instance.SetMenu(MenuManager.RoomMenu); // go into the room
		text_roomName.text = PhotonNetwork.CurrentRoom.Name; // get the room name
		Player[] players = PhotonNetwork.PlayerList;

		foreach (Transform trans in panel_playerListHolder) // reset all of the players
			Destroy(trans.gameObject);

		for (int i = 0; i < players.Count(); i++) // add the players
			Instantiate(prefab_playerListItem, panel_playerListHolder).GetComponent<PlayerListItem>().SetUp(players[i], IsPlayer(players[i]));

		UpdateRoomVisibility();
	}
	public override void OnMasterClientSwitched(Player newMasterClient) {
		UpdateRoomVisibility();
	}
	public void StartGame() {
		GameConfiguration.Instance.ApplySettings();
		PhotonNetwork.LoadLevel(1); // load the game level
	}
	public void JoinRoom(RoomInfo info) {
		SetPlayerName();
		PhotonNetwork.JoinRoom(info.Name);
		MenuManager.Instance.SetMenu(MenuManager.LoadingMenu);
	}
	public void LeaveRoom()  {
		MenuManager.Instance.SetMenu(MenuManager.LoadingMenu);
		PhotonNetwork.LeaveRoom();
	}
	public override void OnLeftRoom() {
		MenuManager.Instance.SetMenu(MenuManager.TitleMenu);
	}
	public override void OnRoomListUpdate(List<RoomInfo> roomList) {
		List<RoomListItem> roomListGO = new List<RoomListItem>(panel_roomListHolder.GetComponentsInChildren<RoomListItem>());

		foreach (RoomInfo room in roomList) {
			if (room.RemovedFromList) { // if the room is removed
				myRoomInfo.Remove(room);
			} else if (!myRoomInfo.Contains(room)) { // if the room is not already instantiated
				myRoomInfo.Add(room);
			}
		}

		UpdateRooms();
	}
	public override void OnPlayerEnteredRoom(Player newPlayer) {
		Instantiate(prefab_playerListItem, panel_playerListHolder).GetComponent<PlayerListItem>().SetUp(newPlayer, IsPlayer(newPlayer));
		button_startGame.GetComponent<Button>().interactable = CanStartGame();
	}
	public override void OnPlayerLeftRoom(Player otherPlayer) {
		button_startGame.GetComponent<Button>().interactable = CanStartGame();
	}
	public override void OnDisconnected(DisconnectCause cause) {
		SceneManager.LoadScene(0);
	}
	private bool IsPlayer(Player player) {
		List<Player> playerlist = PhotonNetwork.PlayerList.ToList();
		int index = playerlist.IndexOf(player);
		return index < 2;
	}
	private bool CanStartGame() => PhotonNetwork.PlayerList.Length >= 2;
	private void UpdateRooms() {
		foreach (Transform trans in panel_roomListHolder)
			Destroy(trans.gameObject);
		foreach (RoomInfo info in myRoomInfo) {
			if (!info.IsOpen) { continue; }
			Instantiate(prefab_roomListItem, panel_roomListHolder).GetComponent<RoomListItem>().SetUp(info);
		}
	}
	private void SetPlayerName() {
		PlayerPrefs.SetString("playername", inputField_playerName.text);
		PhotonNetwork.NickName = inputField_playerName.text + " [" + Random.Range(0, 1000).ToString("0000")+"]";
		if (inputField_playerName.text == string.Empty) {
			PhotonNetwork.NickName = "Guest [" + Random.Range(0, 1000).ToString("0000")+"]";
		}
	}
	private void UpdateRoomVisibility() {
		button_startGame.SetActive(PhotonNetwork.IsMasterClient);
		panel_boardSize.SetActive(PhotonNetwork.IsMasterClient);
	}
}