using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Photon.Pun;
using TMPro;
using Photon.Realtime;

public class GameUI : MonoBehaviourPunCallbacks {
	public static GameUI Inst;
	[SerializeField] private TMP_Text roomName;
	[SerializeField] private RuleInGame rule;
	[SerializeField] private Transform ruleHolder;
	[SerializeField] private TMP_Text[] playerNames;
	[SerializeField] private TMP_Text playerStatus;
	[SerializeField] private TMP_Text numSpectators;
	[SerializeField] private GameObject endScreen;
	[SerializeField] private TMP_Text endScreenText;
	[SerializeField] private Button leaveGame;
	[SerializeField] private TMP_Text turnTimer;
	private int turnTime = 3;
	float currentTurnTime;

	private void Awake() {
		Inst = this;
	}

	private void Start() {
		roomName.text = PhotonNetwork.CurrentRoom.Name;
		endScreen.SetActive(false);
		SetPlayerNames();
		CreateActiveRules();
		leaveGame.onClick.AddListener(delegate{ LeaveGame(); });
		ChaosTimerStartup();
		playerStatus.gameObject.SetActive(GameManager.Inst.GetPlayerIndex() <= 1 && !GameManager.Inst.HasRule(Rules.Chaos));
	}
	private void FixedUpdate() {
		if (!turnTimer.gameObject.activeInHierarchy) { return; }
		if (currentTurnTime <= 0) {
			turnTimer.text = "Move!";
		} else {
			currentTurnTime -= Time.deltaTime;
			turnTimer.text = currentTurnTime.ToString(format: "0.0");
		}
	}
	private void CreateActiveRules() {
		int[] activeRules = GameConfiguration.Instance.GetActiveRules().ToArray();
		(string, string)[] rules = GameConfiguration.Instance.GetRules();
		foreach (int activeRuleIndex in activeRules)
			Instantiate(rule, ruleHolder).SetUp(rules[activeRuleIndex], Thumbnail.Instance.GetRuleThumbnail(activeRuleIndex));
	}

	private void SetPlayerNames() {
		for (int i = 0; i < 2; i++)
			playerNames[i].text = PhotonNetwork.PlayerList[i].NickName;
		if (GameManager.Inst.GetPlayerIndex() == 1) {
			(playerNames[0].text, playerNames[1].text) = (playerNames[1].text, playerNames[0].text);
		}
	}
	public void SetPlayerStatus(bool myTurn) => playerStatus.text = myTurn ? "<color=orange>My Turn</color>" : "<color=purple>Not my turn</color>";

	public void EndGame(bool win) {
		turnTimer.gameObject.SetActive(false);
		endScreen.SetActive(true);
		endScreenText.text = win ? "Victory!" : "Defeat";
		if (GameManager.Inst.GetPlayerIndex() > 1)
			endScreenText.text = "Match Over";
	}
	public float GetTurnTime() => currentTurnTime;
	public void ResetTurnTime() => currentTurnTime = turnTime;
	private void ChaosTimerStartup() {
		turnTimer.gameObject.SetActive(GameManager.Inst.HasRule(Rules.Chaos));
	}
	private void LeaveGame() {
		// GameManager.Inst.PlayerLeave(GameManager.Inst.GetPlayerIndex());
		PhotonNetwork.Disconnect();
	}
	public void SetSpectators(int num) {
		numSpectators.text = "   "+num.ToString() + " Spectators";
	}
	public override void OnDisconnected(DisconnectCause cause) {
		SceneManager.LoadScene(0);
	}
}