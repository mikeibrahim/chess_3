using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using TMPro;

public class PlayerListItem : MonoBehaviourPunCallbacks {
	[SerializeField] private TMP_Text text_playerName;
	Player player;

    public void SetUp(Player player, bool isPlayer) {
		this.player = player;
		text_playerName.text = isPlayer ? player.NickName : player.NickName + " [Spectator]";;
	}

	public override void OnPlayerLeftRoom(Player otherPlayer) {
		if (player == otherPlayer) {
			Destroy(gameObject);
		}
	}

	public override void OnLeftRoom() {
		Destroy(gameObject);
	}
}
